from django.contrib import admin

# Register your models here.
from .models import Grupo, Bicho

admin.site.register(Grupo)
admin.site.register(Bicho)
