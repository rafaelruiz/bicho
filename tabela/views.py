from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.template import loader

from django.views import generic

from .models import Grupo, Bicho

# Create your views here.

class IndexView(generic.ListView):
  template_name = 'tabela/index.html'
  context_object_name = 'grupo_list'
  
  def get_queryset(self):
    return Grupo.objects.order_by('numero')
  
def index(request):
  grupos = Grupo.objects.all()
  lista_grupos = []
  item = []
  ii=0
  for g in grupos:
    ii+=1

    if ii < 6:
      item.append(g)
    else:
      lista_grupos.append(item)
      item = [g]
      ii=1
  lista_grupos.append(item)
    
  tpl = loader.get_template('tabela/index.html')
  context = {
    'grupo_list': lista_grupos,
  }
  return HttpResponse(tpl.render(context,request))
  
def detail(request,grupo_id):
  try:
    grupo = Grupo.objects.get(numero=grupo_id)
    numeros = Bicho.objects.filter(grupo = grupo.id)
  except Grupo.DoesNotExist:
    raise Http404("Grupo nao existe")
  
  #import pdb;pdb.set_trace()
  return render(request, 'tabela/detail.html', {'grupo': grupo, 'numeros': numeros})
  
def result(request,grupo_id):
  return HttpResponse('vc esta vendo os resultados do grupo %d' % grupo_id)
  
def vote(request,grupo_id):
  grupo = get_object_or_404(Grupo,numero=grupo_id)
  try:
    numero_choice = Bicho.objects.get(numero=request.POST['numero'])
  except (KeyError, Bicho.DoesNotExist):
    return render(request, 'tabela/detail.html', {
      'grupo': grupo,
      'error_message': 'Vc nao selecionou uma opcao'
    })    
  return HttpResponse('Vc esta votando no grupo %s' % grupo_id)
  