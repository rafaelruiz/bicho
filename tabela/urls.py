from django.urls import path
from . import views

app_name = 'bicho'
urlpatterns = [
  path('', views.index, name='index'),
  path('<int:grupo_id>/', views.detail, name='detail'),
  path('<int:grupo_id>/result/', views.result, name='result'),
  path('<int:grupo_id>/vote/', views.vote, name='vote'),  
]