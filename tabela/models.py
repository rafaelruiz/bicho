from django.db import models

# Create your models here.

class Grupo(models.Model):
  numero = models.IntegerField(default=0)
  nome = models.CharField(max_length=100)
  imagem = models.CharField(max_length=255)
  
  def __str__(self):
    return self.nome
  
class Bicho(models.Model):
  grupo = models.ForeignKey(Grupo,on_delete=models.CASCADE)
  numero = models.IntegerField(default=0)

  def __str__(self):
    return "%d" % self.numero